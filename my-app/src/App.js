import './App.css';
import React from 'react';
import Dashboard from './Dashboard';

function App() {
  return (
    <React.Fragment>
      <Dashboard/>
    </React.Fragment>
  );
}

export default App;
