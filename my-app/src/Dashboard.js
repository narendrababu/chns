import React,{useEffect, useState} from 'react'
import data from "./data.json"
import Row from "./Row"

function Dashboard() {
    const [unadjusted1,setUnadjusted1] = useState(0);
    const [unadjusted2,setUnadjusted2] = useState(0);
    const [unadjusted3,setUnadjusted3] = useState(0);
    const [unadjusted4,setUnadjusted4] = useState(0);
    const [unadjusted1comment,setUnadjusted1comment] = useState("Equal");
    const [unadjusted2comment,setUnadjusted2comment] = useState("Equal");
    const [unadjusted3comment,setUnadjusted3comment] = useState("Equal");
    const [unadjusted4comment,setUnadjusted4comment] = useState("Equal");
    const [select1,setSelect1] = useState("")
    const [select2,setSelect2] = useState("")
    const [select3,setSelect3] = useState("")
    const [select4,setSelect4] = useState("")
    const [values,setValues] = useState({});
    const [totalbetnovatevalue,setTotalBetnovate] = useState(0)
    const [adjustedbetnovatetotal,setTotalAdjustedBetnovate] = useState(0)
    const [totaladjustedbact,setTotalAdjustedBact] = useState(0)
    const [totals,setTotals] = useState({})
    // useEffect(()=>{
    //     setSelect1(document.querySelector(".Unadjusted__Budget__UN__1").textContent)
    //     // setSelect2(document.querySelector(".Unadjusted__Budget__UN__2").textContent)
    //     // setSelect3(document.querySelector(".Unadjusted__Budget__UN__3").textContent)
    //     // setSelect4(document.querySelector(".Unadjusted__Budget__UN__4").textContent)
    // },[])
    // console.log(data,"data")
    // const handleChange = (event) =>{
        
    //     setSelect1(document.querySelector(".Unadjusted__Budget__UN__1").textContent)
    //     // setSelect2(document.querySelector(".Unadjusted__Budget__UN__2").textContent)
    //     // setSelect3(document.querySelector(".Unadjusted__Budget__UN__3").textContent)
    //     // setSelect4(document.querySelector(".Unadjusted__Budget__UN__4").textContent)

    //     console.log(event)
    //     if(event.target.classList.contains("one")){
    //         setUnadjusted1(event.target.value)
    //     }
    //     if(event.target.classList.contains("two")){
    //         setUnadjusted2(event.target.value)
    //     }
    //     if(event.target.classList.contains("three")){
    //         setUnadjusted3(event.target.value)
    //     }
    //     if(event.target.classList.contains("four")){
    //         setUnadjusted4(event.target.value)
    //     }        
    // }
    
    useEffect(()=>{
        let totalBetnovate = 0
        let totalAdjustedBetnovate = 0
        let totalAdjustedBact = 0
        const some  = document.querySelectorAll(".Betnovate")
        let adjustedBetnovate = document.querySelectorAll("#Betnovate__adjusted")
        // console.log(some,"someeeeeee")
        
        let adjusted_T_Bact_adjusted = document.querySelectorAll("#T-Bact__adjusted")
        for(let i=0;i<some.length;i++){
            console.log("went inside")
            totalBetnovate+=Number(some[i].value)
            totalAdjustedBetnovate+=Number(adjustedBetnovate[i].textContent)
            totalAdjustedBact+=Number(adjusted_T_Bact_adjusted[i].textContent)
        }
        if(totalBetnovate!=0){
            setTotals({...totals,"totalBetnovate":totalBetnovate,"totalAdjustedBetnovate":totalAdjustedBetnovate,"totalAdjustedBact":totalAdjustedBact})
        }
        
    },[values])
    // setTotalAdjustedBetnovate(totalAdjustedBetnovate)
    // setTotalAdjustedBact(totalAdjustedBact)
    const handleChange = (e) => {    
        const {name,value} = e.target;
        // console.log(name,value)
            setValues({...values,
                [name]:value
            })
    }

    
  return (
    <>
        <section class="flex flex-col">
            <div class="overflow-x-auto sm:-mx-6 lg:-mx-8">
                <div class="py-2 inline-block min-w-full sm:px-6 lg:px-8">
                    <div class="overflow-hidden">
                        <table class="min-w-full">
                            <thead class="border-b">
                                <tr>
                                    <th scope="col" class="text-sm font-medium text-gray-900 px-1 py-2 text-left">
                                        ABMCODE
                                    </th>
                                    <th scope="col" class="text-sm font-medium text-gray-900 px-1 py-2 text-left">
                                        ABMNAME
                                    </th>
                                    <th scope="col" class="text-sm font-medium text-gray-900 px-1 py-2 text-left">
                                        REPCODE
                                    </th>
                                    <th scope="col" class="text-sm font-medium text-gray-900 px-1 py-2 text-left">
                                        REPHQ
                                    </th>
                                    <th scope="col" class="text-sm font-medium text-gray-900 px-1 py-2 text-left">
                                        PRODCODE
                                    </th>
                                    <th scope="col" class="text-sm font-medium text-gray-900 px-1 py-2 text-left">
                                        PRODDESC
                                    </th>
                                    <th scope="col" class="text-sm font-medium text-gray-900 px-1 py-2 text-left">
                                        BRAND
                                    </th>
                                    <th scope="col" class="text-sm font-medium text-gray-900 px-1 py-2 text-left">
                                        MIN
                                    </th>
                                    <th scope="col" class="text-sm font-medium text-gray-900 px-1 py-2 text-left">
                                        MAX
                                    </th>
                                    <th scope="col" class="text-sm font-medium text-gray-900 px-1 py-2 text-left">
                                        Unadjusted Budget UN
                                    </th>
                                    <th scope="col" class="text-sm font-medium text-gray-900 px-1 py-2 text-left">
                                        Delta Adjustment
                                    </th>
                                    <th scope="col" class="text-sm font-medium text-gray-900 px-1 py-2 text-left">
                                        Adjusted Budget UN
                                    </th>
                                    <th scope="col" class="text-sm font-medium text-gray-900 px-1 py-2 text-left">
                                        Adjusted RCI
                                    </th>
                                    <th scope="col" class="text-sm font-medium text-gray-900 px-1 py-2 text-left">
                                        Comments
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <Row totals={totals} totaladjustedbact={totaladjustedbact} adjustedbetnovatetotal={adjustedbetnovatetotal} totalbetadinvalue={totalbetnovatevalue} data={data} handleChange = {handleChange} values={values} unadjusted1={unadjusted1} select1={select1}/>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
    </>
  )
}

export default Dashboard