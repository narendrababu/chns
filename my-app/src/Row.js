import React,{useEffect, useState} from 'react'

function Row(props) {
    console.log(props,"ppppppppppppppppppp")
    const [adjustrci,setAdjustRCI] = useState([])
    let allBetnovateAdjustedBudget = document.querySelectorAll(`#Betnovate__adjusted`)
    let alltbactcreamAdjustedBudget = document.querySelectorAll(`#T-Bact__adjusted`)
    let clubAll = [...allBetnovateAdjustedBudget,...alltbactcreamAdjustedBudget]
    let clubbedAllValues = [];
    let AdjustedRCI = [];
    for(let i=0;i<clubAll.length;i++){
        clubbedAllValues.push(Number(clubAll[i]?.textContent))
    }
    for(let i=0;i<allBetnovateAdjustedBudget.length;i++){
        console.log(clubbedAllValues,props?.totals?.totalAdjustedBetnovate,"vvvvvvvvvvvvvvvvvvv")
        AdjustedRCI.push((Number(clubbedAllValues[i])/Number(props?.totals?.totalAdjustedBetnovate))*100)
        
    }
    for(let i=allBetnovateAdjustedBudget.length;i<clubbedAllValues.length;i++){
        console.log(clubbedAllValues[i],"wowwwwwwwwwww")
        // console.log(props?.totals?.totalAdjustedBact,"wowwwwwwwwwwwww")
        AdjustedRCI.push((Number(clubbedAllValues[i])/Number(props?.totals?.totalAdjustedBact))*100)
    }
    console.log(props?.total?.totalAdjustedBetnovate,"xxxxxxxxxxxxxxxxxxxx")
    console.log(AdjustedRCI,"outttttttttttttttttttttttttttt")
    useEffect(()=>{
        if(props?.totals?.totalAdjustedBetnovate>0||props?.totals?.totalAdjustedBact>0){
            console.log(AdjustedRCI,"adjustedddddddddddddRCIIIIIIIIIIIIIIIIIII")
            setAdjustRCI(AdjustedRCI);
        }
    },[props.values])
    
    
  return (
    <>
       {props.data.map((eachRow,i)=>(
        <>
            <tr class="border-b">
                <td class="text-sm text-gray-900 font-light px-0.2 py-2 whitespace-nowrap">
                    {eachRow.RBMCODE}
                </td>
                <td class="text-sm text-gray-900 font-light px-0.2 py-2 whitespace-nowrap">
                    {eachRow.RBMNAME}
                </td>
                <td class="text-sm text-gray-900 font-light px-0.2 py-2 whitespace-nowrap">
                    {eachRow.ABMCODE}
                </td>
                <td class="text-sm text-gray-900 font-light px-0.2 py-2 whitespace-nowrap">
                    {eachRow.ABMNAME}
                </td>
                <td class="text-sm text-gray-900 font-light px-0.2 py-2 whitespace-nowrap">
                    {eachRow.PRODCODE}
                </td>
                <td class="prod__description text-sm text-gray-900 font-light px-0.2 py-2 w-2 whitespace-pre">
                    {eachRow.PRODDESC}
                </td>
                <td class="text-sm text-gray-900 font-light px-0.2 py-2 whitespace-nowrap">
                    {eachRow.BRAND}
                </td>
                <td class="text-sm text-gray-900 font-light px-0.2 py-2 whitespace-nowrap">
                    {(parseFloat(eachRow.Median).toFixed(4))*100}
                </td>
                <td class="text-sm text-gray-900 font-light px-0.2 py-2 whitespace-nowrap">
                    {((eachRow.Max).toFixed(4))*100}
                </td>
                <td class="Unadjusted__Budget__UN__1 text-sm text-gray-900 font-light px-0.2 py-2 whitespace-nowrap">
                    {eachRow["BUD Unadjusted"].toFixed(2)}
                </td>
                <td class="text-sm text-gray-900 font-light px-0.2 py-2 whitespace-nowrap">
                    <input type="text" className={`${eachRow.BRAND}`} name={`data-${i}`} onChange={props.handleChange} value={props.values[`data-${i}`]==null?0:props.values[`data-${i}`]}/>
                    <div>{props?.totals?.totalAdjustedBetnovate>0?"Beyond RBM Budget":""}</div>
                    <div>{props?.totals?.totalAdjustedBetnovate<0?"Below RBM Budget":""}</div>
                </td>
                <td id={`${eachRow.BRAND}__adjusted`} className="text-sm text-gray-900 font-light px-0.2 py-2 whitespace-nowrap">
                    {(Number(props.values[`data-${i}`]==null?0:props.values[`data-${i}`])+Number(eachRow["BUD Unadjusted"])).toFixed(2)}
                </td>
                <td class="text-sm text-gray-900 font-light px-0.2 py-2 whitespace-nowrap">
                    {adjustrci[i]}
                </td>
                <td class="text-sm text-gray-900 font-light px-0.2 py-2 whitespace-nowrap">
                    {(Number(adjustrci[i])<Number(eachRow.Max))&& (Number(adjustrci[i])>Number(eachRow.Median))?"with in limit":"out"}
                </td>
            </tr>
        </>
        ))}
    </>
  )
}

export default Row